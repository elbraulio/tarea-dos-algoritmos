package sufixtree;

/**
 * Created by elbraulio on 06-12-16.
 */
public class SuffixArc {

    private LinkedChar firstChar;
    private SuffixNode node = new SuffixNode();

    public void insert(char[] sequence) {

        LinkedChar lastEqual = compareChar(firstChar, sequence, 1);
        if (lastEqual.getNext().isNull()) {
            lastEqual.insert(sequence, lastEqual.getPosition() + 1);
        } else {
            LinkedChar rest = lastEqual.getNext();
            lastEqual.setNext(new NullLinkedChar());

            SuffixNode newNode = new SuffixNode();
            SuffixArc arc1 = new SuffixArc();
            SuffixArc arc2 = new SuffixArc();

            // Resto de la secuencia de este arco
            rest.updatePositions(0);
            arc1.setSequence(rest);
            arc1.setNode(node);

            //Resto de la secuencia insertada
            NodeLinkedChar head = new NodeLinkedChar(sequence[lastEqual.getPosition() + 1]);
            head.insert(sequence, lastEqual.getPosition() + 2);
            arc2.setSequence(head);

            newNode.insert(arc1);
            newNode.insert(arc2);

            node = newNode;
        }

    }

    /**
     * Recorre la lista enlazada hasta encontrar al distinto y lo retorna
     * @param linkedChar
     * @param sequence
     * @param index
     * @return
     */
    private LinkedChar compareChar(LinkedChar linkedChar, char[] sequence, int index) {
        if (linkedChar.getNext().getValue() == sequence[index]) {
            return compareChar(linkedChar.getNext(), sequence, index + 1);
        } else {
            return linkedChar;
        }
    }

    public char getFirstChar() {
        return firstChar.getValue();
    }

    public void setSequence(LinkedChar linked) {
        firstChar = linked;
    }

    public void setNode(SuffixNode node) {
        this.node = node;
    }
}
