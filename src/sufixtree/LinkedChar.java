package sufixtree;

/**
 * Created by elbraulio on 06-12-16.
 */
public interface LinkedChar {

    void insert(char[] c, int pos);

    boolean isNull();

    char getValue();

    LinkedChar getNext();


    int getPosition();

    void setNext(NullLinkedChar nullLinkedChar);

    void updatePositions(int i);
}
