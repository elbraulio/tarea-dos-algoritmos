package sufixtree;

/**
 * Created by elbraulio on 06-12-16.
 */
public class NullLinkedChar implements LinkedChar {


    @Override
    public void insert(char[] c, int pos) {

    }

    @Override
    public boolean isNull() {
        return true;
    }

    @Override
    public char getValue() {
        return 0;
    }

    @Override
    public LinkedChar getNext() {
        return null;
    }

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    public void setNext(NullLinkedChar nullLinkedChar) {

    }

    @Override
    public void updatePositions(int i) {

    }
}
