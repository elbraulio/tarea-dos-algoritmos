package sufixtree;

import java.util.HashMap;

/**
 * Created by elbraulio on 06-12-16.
 */
public class SuffixNode{

    /*
     * Contiene todas las aristas unidas a este nodo, donde
     * la llave de cada una es el primer caracter de estas.
     */
    private HashMap<Character, SuffixArc> hash;

    public SuffixNode() {
        hash = new HashMap();
    }

    /**
     * Inserta una secuencia de caracteres en la arista que corresponda.
     * Para esto primero se busca la arista cuyo primer caracter coincida con
     * la secuencia entregada, luego se le inserta. Si no existe coincidencia,
     * se crea una arista nueva y alli se inserta la secuencia.
     * @param sequence: es la secuencia a insertar
     */
    public void insert(char[] sequence) {

        if(hash.containsKey(sequence[0])) {
            SuffixArc arc = hash.get(sequence[0]);
            arc.insert(sequence);
        } else {
            SuffixArc newArc = new SuffixArc();
            newArc.insert(sequence);
            hash.put(sequence[0], newArc);
        }

    }

    public void insert(SuffixArc arc) {
        hash.put(arc.getFirstChar(), arc);
    }
}
