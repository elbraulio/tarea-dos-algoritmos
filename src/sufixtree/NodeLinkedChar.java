package sufixtree;

/**
 * Created by elbraulio on 06-12-16.
 */
public class NodeLinkedChar implements LinkedChar {

    private char value;
    private int position;
    private LinkedChar next;

    public NodeLinkedChar(char c) {
        value = c;
        next = new NullLinkedChar();
        position = 0;
    }

    public NodeLinkedChar(char c, int pos) {
        value = c;
        next = new NullLinkedChar();
        position = pos;
    }


    @Override
    public void insert(char[] c, int start) {

        if(start < c.length) {
            next = new NodeLinkedChar(c[start], position + 1);
            next.insert(c, start + 1);
        }


    }

    @Override
    public boolean isNull() {
        return false;
    }

    @Override
    public char getValue() {
        return value;
    }

    @Override
    public LinkedChar getNext() {
        return next;
    }

    @Override
    public int getPosition() {
        return position;
    }

    @Override
    public void setNext(NullLinkedChar nullLinkedChar) {
        next = nullLinkedChar;
    }

    @Override
    public void updatePositions(int i) {
        position = i;
        next.updatePositions(i + 1);
    }
}
