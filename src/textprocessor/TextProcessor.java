package textprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by elbraulio on 06-12-16.
 */
public interface TextProcessor {

    void processText(File file) throws IOException;

    String getProcessedText();

}
