package textprocessor;

import java.io.*;
import java.util.regex.Pattern;


/**
 * Created by elbraulio on 06-12-16.
 */
public class OnlyLetters implements TextProcessor{

    private StringBuilder textBuilder = new StringBuilder();

    /**
     * Elimina todos los caracteres que no sean A-Z o a-b o 0-9.
     * @param file: es el archivo que contiene el texto a procesar
     * @throws IOException
     */
    @Override
    public void processText(File file) throws IOException {
        Pattern aceptableCase = Pattern.compile("[a-zA-Z0-9]");
        FileReader inputStream = new FileReader(file);
        int i;
        char c;
        String character;
        while ((i = inputStream.read()) != -1) {
            //transforma el int c a un char y luego a un string
            c = (char) i;
            character = Character.toString(c);
            //si es una mayúscula, la cambia a minuscula y la agrega
            if(aceptableCase.matcher(character).matches()) {
                    textBuilder.append(character.toLowerCase());
            }
        }
    }

    @Override
    public String getProcessedText() {
        return textBuilder.toString();
    }
}
