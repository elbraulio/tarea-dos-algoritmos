package launcher;

import textprocessor.OnlyLetters;
import textprocessor.TextProcessor;

import java.io.File;
import java.io.IOException;

/**
 * Created by elbraulio on 06-12-16.
 */
public class Launcher {
    public static void main(String args[]) throws IOException {
        TextProcessor txtprcs = new OnlyLetters();
        txtprcs.processText(new File("/home/elbraulio/IdeaProjects/tarea-dos-algoritmos/lorem.txt"));
        System.out.println(txtprcs.getProcessedText());

    }
}
